const month = {
  "Januari": ["januari", "january", "jan", "01", "1"],
  "Februari": ["februari", "february", "febuari", "feb", "02", "2"],
  "Maret": ["maret", "march", "mar", "mrt", "03", "3"],
  "April": ["april", "apr", "04", "4"],
  "Mei": ["mei", "may", "05", "5"],
  "Juni": ["juni", "june", "jun", "06", "6"],
  "Juli": ["juli", "july", "jul", "07", "7"],
  "Agustus": ["agustus", "august", "agust", "agst", "aug", "agu", "ags", "agsts", "08", "8"],
  "September": ["september", "sept", "sep", "sptmbr", "09", "9"],
  "Oktober": ["october", "oktober", "oct", "okt", "oktbr", "10"],
  "November": ["november", "nopember", "nvmbr", "npmbr", "novem", "nopem", "nove", "nope", "nov", "nop", "nvmbr", "11"],
  "Desember": ["desember", "december", "dec", "des", "dsmbr", "12"]
};
const reverse_month = {
  "Januari": ["1"],
  "Februari": ["2"],
  "Maret": ["3"],
  "April": ["4"],
  "Mei": ["5"],
  "Juni": ["6"],
  "Juli": ["7"],
  "Agustus": ["8"],
  "September": ["9"],
  "Oktober": ["10"],
  "November": ["11"],
  "Desember": ["12"]
};
let monthNat = [];

for (let currMonth in month) {
  monthNat.push(month[currMonth].join("|"));
}

let monthStr = monthNat.join("|");
let monthDigit = "0[1-9]|1[1-2]|[1-9]";
let dateDigit = "0[1-9]|[1-2][0-9]|3[0-1]|[1-9]";
let yearDigit = "[1-2][0-9]{3}|[0-9]{2}";
let otherCharacter = "[\\s|\\-|\/|\.|,]*";
let str = "[a-zA-Z]*";

const formats = [
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",
  "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",

  "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",

  "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",
  "\\b" + str + "\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",

  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",
  "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",

  "\\b" + str + "\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b" + str + "\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",

  "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",
  "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",
  "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + dateDigit + ")\\b",

  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthStr + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + monthDigit + ")\\b",
  "\\b(" + dateDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + monthStr + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",
  "\\b(" + monthDigit + ")\\b" + otherCharacter + "\\b(" + yearDigit + ")\\b",

  "\\b" + str + "\\b(" + dateDigit + ")\\b",
  "\\b" + str + "\\b(" + monthDigit + ")\\b",
  "\\b" + str + "\\b(" + monthStr + ")\\b",
  "\\b" + str + "\\b(" + yearDigit + ")\\b",

  "\\b(" + dateDigit + ")\\b(" + monthDigit + ")\\b(" + yearDigit + ")\\b",
  "\\b(" + dateDigit + ")(" + monthStr + ")(" + yearDigit + ")\\b",

];

let regexes = formats;

function isLeapYear(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}

function getMonthName(keyword) {
  for (let currMonth in month) {
    if (month[currMonth].indexOf(keyword.toLowerCase()) !== -1) {
      monthName = currMonth;
    }
  }
  return monthName;
}

function parse(message, context) {
  let content = (message.content).toLowerCase() || '';
  if (!message || !message.type || message.type !== "text") {
    return null;
  }

  let match = null;

  for (let i = 0; i < regexes.length; i++) {
    match = content.match(regexes[i]);

    if (match) {
      // return match.toString();
      match.shift();

      for (let i = 0; i < match.length; i++) {
        if (!(/^\d+$/.test(match[i]))) {
          for (let currMonth in month) {
            if (month[currMonth].indexOf(match[i].toLowerCase()) !== -1) {
              match[i] = currMonth;
            }
          }
        }
      }

      // arrange date order and reverse matching
      let arrayDate = [];
      let tempYear = '';
      let tempDay = '';
      let tempMonth = [];
      for (let i = 0; i < 3; i++) {
        if (parseInt(match[i]) >= 1000 || (parseInt(match[i]) >= 40 && parseInt(match[i]) <= 99)) {
          if (match[i] >= 40 && match[i] <= 99) {
            tempYear = "19" + match[i];
          } else {
            tempYear = match[i];
          }
        } else if (parseInt(match[i]) > 12 && parseInt(match[i]) <= 31) {
          tempDay = match[i];
        } else if (parseInt(match[i]) <= 12) {
          if (tempDay || context.correctedDay) {
            let keyword = match[i].toString();
            monthName = getMonthName(keyword)
            tempMonth.push(monthName);
          } else {
            tempDay = match[i];
          }
        } else {
          let keyword = match[i];
          tempMonth.push(keyword);
        }
      }
      arrayDate['day'] = tempDay ? parseInt(tempDay) : 0;
      arrayDate['month'] = tempMonth ? tempMonth[0] : 0;
      arrayDate['year'] = tempYear ? parseInt(tempYear) : 0;

      // checking day value in context and arrayDate
      let dayNumber = '';
      if (context.correctedDay) {
        dayNumber = parseInt(context.correctedDay);
      } else {
        dayNumber = parseInt(arrayDate['day']);
      }
      // giving flag to day
      if (dayNumber < 32) {
        arrayDate['flagDay'] = true;
      } else {
        arrayDate['flagDay'] = 'invalid day';
      }


      // checking month value in context and arrayDate
      let monthNumber = '';
      if (context.correctedMonth) {
        monthNumber = parseInt(reverse_month[context.correctedMonth]);
      } else {
        monthNumber = parseInt(reverse_month[arrayDate['month']]);
      }
      // giving flag to odds/even month
      if (!monthNumber || monthNumber == 0) {
        arrayDate['flagMonth'] = 'invalid month';
      } else {
        if (monthNumber <= 7) {
          if (monthNumber % 2 == 0 && arrayDate['day'] > 30) {
            arrayDate['month'] = null;
            arrayDate['flagMonth'] = 'invalid day or month';
          } else {
            arrayDate['month'] = arrayDate['month'];
            arrayDate['flagMonth'] = true;
          }
        } else if (monthNumber > 7 && monthNumber <= 12) {
          if (monthNumber % 2 == 1 && arrayDate['day'] > 30) {
            arrayDate['month'] = null;
            arrayDate['flagMonth'] = 'invalid day or month';
          } else {
            arrayDate['month'] = arrayDate['month'];
            arrayDate['flagMonth'] = true;
          }
        }


      }

      // checking year value in context and arrayDate
      let yearNumber = '';
      let currentYear = (new Date()).getFullYear();
      if (context.correctedYear) {
        yearNumber = parseInt(context.correctedYear);
      } else {
        yearNumber = parseInt(arrayDate['year']);
      }
      // giving flag to year
      if (yearNumber == 0 || !yearNumber) {
        arrayDate['year'] = null;
        arrayDate['flagYear'] = 'invalid year';
      } else if (yearNumber > currentYear) {
        arrayDate['year'] = null;
        arrayDate['flagYear'] = 'not born yet';
      } else if (yearNumber >= 2010 && yearNumber <= currentYear) {
        arrayDate['year'] = null;
        arrayDate['flagYear'] = 'too young';
      } else if (yearNumber <= 1940) {
        arrayDate['year'] = null;
        arrayDate['flagYear'] = 'too old';
      } else {

        // leap year
        if ((monthNumber == 2) && (dayNumber > 28)) {
          if (isLeapYear(yearNumber)) {
            arrayDate['year'] = yearNumber;
            arrayDate['flagYear'] = true;
          } else {
            arrayDate['year'] = null;
            arrayDate['flagYear'] = 'not leap year';
          }
        }

      }


      // return match.join("-");
      var obj = {
        day: arrayDate['day'],
        flagDay: arrayDate['flagDay'],
        month: arrayDate['month'],
        flagMonth: arrayDate['flagMonth'],
        year: arrayDate['year'],
        flagYear: arrayDate['flagYear'],
      };
      // Return it
      return obj;
    }
  }
  return null;
}
