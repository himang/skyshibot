function toDoList(message, context, data) {
  var todo;
  if (!context.toDo_today) {
    todo = [];
  } else {
    todo = context.toDo_today;
  }
  todo.push(message.content);

  var obj = {
    todoList: todo
  };
  return obj;
}

function chooseToDo(message, context, data) {
  var i = message.content - 1;
  var taskId = data.data[i].id;
  var detail = data.data[i].detail;
  var obj = {
    taskId: taskId,
    detail: detail
  }
  return obj;
}

function updateToDo(message, context, data) {
  var update = [];
  var j = 1;
  var numb = [];
  for (var i in data.toDo_today) {
    if (j != data.todoNow_ID) {
      update.push(data.toDo_today[i]);
      numb.push(j);
    }
    j++;
  }
  data.todoNow_ID = [];

  data.toDo_today = [];
  data.toDo_today = update;

  data.todoNumb = [];
  data.todoNumb = numb;

  data.toDoSum = data.toDo_today.length;
  return update;
}
